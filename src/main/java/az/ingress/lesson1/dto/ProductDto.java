package az.ingress.lesson1.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductDto {
    String name;
    Integer price;
    ProductEnum category;
    String description;
    ProductDetailsDto productDetails;
    Set<CategoryDto> categories = new HashSet<>();
}
