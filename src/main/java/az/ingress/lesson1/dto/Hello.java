package az.ingress.lesson1.dto;

import jakarta.annotation.PostConstruct;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class Hello {

    String name = "Hello";
    @PostConstruct
    public  void Init(){
        log.info("Hello bin initialized");
    }
}
