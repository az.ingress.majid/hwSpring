package az.ingress.lesson1.dto;

import az.ingress.lesson1.model.Products;

import java.util.List;
import java.util.stream.Collectors;

public class ProductMapper {
    public static ProductDto toDto(Products product) {
        if (product == null) {
            return null;
        }

        ProductDto dto = new ProductDto();

        dto.setName(product.getName());
        dto.setPrice(product.getPrice());
        // Map other fields as necessary

        return dto;
    }

    public static List<ProductDto> toDtoList(List<Products> products) {
        return products.stream().map(ProductMapper::toDto).collect(Collectors.toList());
    }

}
