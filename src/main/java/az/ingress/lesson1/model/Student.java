package az.ingress.lesson1.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long ID;
    String Name;
    @ManyToMany(fetch = FetchType.EAGER)
            @JoinTable(name="student_authority",
                    joinColumns = @JoinColumn(name = "student_id", referencedColumnName = "id"),
                    inverseJoinColumns = @JoinColumn(name = "authority_id", referencedColumnName = "id"))

    @JsonIgnore
    @EqualsAndHashCode.Exclude
    Set<Authority> authorities = new HashSet<>();
}
