package az.ingress.lesson1.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Authority {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long ID;

    @Enumerated(EnumType.STRING)
    Role role;
    @ManyToMany(mappedBy = "authorities")
    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    Set<Student> students = new HashSet<>();

}
