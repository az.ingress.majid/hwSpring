package az.ingress.lesson1;

import az.ingress.lesson1.dto.BookResponseDto;
import az.ingress.lesson1.model.Address;
import az.ingress.lesson1.model.Book;
import az.ingress.lesson1.model.Products;
import az.ingress.lesson1.repository.AddressRepository;
import az.ingress.lesson1.repository.BookRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Transient;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.domain.Specification;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

@SpringBootApplication
@RequiredArgsConstructor
@EnableCaching
public class Lesson1Application implements CommandLineRunner  {
	private final EntityManagerFactory emf;
	private  final BookRepository bookRepo;
	private  final AddressRepository addressRepository;

	public static void main(String[] args) {
		SpringApplication.run(Lesson1Application.class, args);


	}


	@Override
	public void run(String... args) throws Exception {
		Specification<Book> specification = (root,query, criteriaBuilder) -> criteriaBuilder.equal(root.get("name"),"1");
		Specification<Book> specification3 = (root,query, criteriaBuilder) -> {
			Join<Book, Address> join = root.join("address", JoinType.INNER);
			return  criteriaBuilder.equal(join.get("id"),3);
		};

		var result = bookRepo.findAll(specification.and(specification3));

	}
}
