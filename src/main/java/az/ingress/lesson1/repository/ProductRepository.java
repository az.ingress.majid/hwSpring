package az.ingress.lesson1.repository;

import az.ingress.lesson1.model.Book;
import az.ingress.lesson1.model.Products;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Products, Integer> {

    List<Products> findAllByPriceGreaterThanEqual(Integer priceFrom);

    @Override
    @EntityGraph(attributePaths = {"Categories"})
    List<Products> findAll();
    List<Products> findAllByPriceLessThanEqual(Integer priceTo);
    @Query(value = "select p.category, count(*) as count from products p group by category", nativeQuery = true)
    Optional<List<ProductProjection>> orderByCategory();
    List<Products> findAllByPriceGreaterThanEqualAndPriceLessThanEqual(Integer priceFrom, Integer priceTo);


}
