package az.ingress.lesson1.repository;

import az.ingress.lesson1.model.Address;
import az.ingress.lesson1.model.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority, Integer> {
}
