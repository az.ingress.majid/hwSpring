package az.ingress.lesson1.repository;

public interface ProductProjection {
    String getCategory();
    Integer getCount();

}
