package az.ingress.lesson1.repository;

import az.ingress.lesson1.model.Account;
import az.ingress.lesson1.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository  extends JpaRepository<Account, Integer> {
}
