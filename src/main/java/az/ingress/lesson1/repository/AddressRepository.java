package az.ingress.lesson1.repository;

import az.ingress.lesson1.model.Address;
import az.ingress.lesson1.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Integer> {
}
