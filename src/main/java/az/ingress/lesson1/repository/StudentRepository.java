package az.ingress.lesson1.repository;

import az.ingress.lesson1.model.Products;
import az.ingress.lesson1.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface StudentRepository extends JpaRepository<Student, Integer>, JpaSpecificationExecutor {
}
