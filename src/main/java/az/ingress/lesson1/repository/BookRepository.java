package az.ingress.lesson1.repository;

import az.ingress.lesson1.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface BookRepository extends JpaRepository<Book, Integer>, JpaSpecificationExecutor<Book> {
    List<Book> findAllByPageCountGreaterThan(int page);
    @Query(value = "select b.id from book b where page_count= :pageCount", nativeQuery = true)
    Optional<BookProjection> findCustom(Integer pageCount);
}
