package az.ingress.lesson1.controller;

import az.ingress.lesson1.dto.ProductDto;
import az.ingress.lesson1.model.Products;
import az.ingress.lesson1.repository.ProductProjection;
import az.ingress.lesson1.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;
    @PostMapping()
    public  void CreateProduct(@RequestBody ProductDto productDto){
        productService.create(productDto);

    }
    @DeleteMapping()
    public  void DeleteProduct( @RequestParam(value = "id", required = false)  int productID){
        productService.delete(productID);

    }
    @GetMapping
    public ResponseEntity<List<ProductDto>> GetProducts( @RequestParam(value = "priceFrom", required = false) Integer priceFrom,  @RequestParam(value = "priceTo", required = false) Integer priceTo){
        var result = productService.GetProducts(priceFrom,priceTo);
       return ResponseEntity.ok(result);
    }
    @GetMapping("getproduct")
    public ResponseEntity<Products> GetProducts(@RequestParam(value = "id", required = false) Integer id){
        var result = productService.GetProduct(id);
        return ResponseEntity.ok(result);
    }

    @GetMapping("count")
    public ResponseEntity<List<ProductProjection>> GetProductCount( ){
        var result = productService.GetProductsCount();
        return ResponseEntity.ok(result);
    }
}
