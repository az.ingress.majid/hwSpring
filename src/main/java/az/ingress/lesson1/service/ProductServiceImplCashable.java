package az.ingress.lesson1.service;

import az.ingress.lesson1.dto.ProductDto;
import az.ingress.lesson1.dto.ProductMapper;
import az.ingress.lesson1.model.Authority;
import az.ingress.lesson1.model.Products;
import az.ingress.lesson1.repository.AuthorityRepository;
import az.ingress.lesson1.repository.ProductProjection;
import az.ingress.lesson1.repository.ProductRepository;
import az.ingress.lesson1.repository.StudentRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;

@Service
@Primary
@Slf4j
@RequiredArgsConstructor
public class ProductServiceImplCashable implements  ProductService {

    private final ProductRepository productRepository;
    private  final AuthorityRepository authorityRepository;
    private  final StudentRepository studentRepository;




    @Override
    public List<ProductProjection> GetProductsCount() {
       return   productRepository.orderByCategory().orElseThrow(()-> new RuntimeException("not_found"));
    }

    @Override
    public List<ProductDto> GetProducts(Integer priceFrom, Integer priceTo) {
        List<Products> products;
        if (priceFrom == null && priceTo == null) {
            products=  productRepository.findAll();
            var z = products.get(5).getCategories();
            var t = 5;
        } else if (priceFrom == null) {
            products=   productRepository.findAllByPriceLessThanEqual(priceTo);
        } else if (priceTo == null) {
            products=  productRepository.findAllByPriceGreaterThanEqual(priceFrom);
        } else {
            products=  productRepository.findAllByPriceGreaterThanEqualAndPriceLessThanEqual(priceFrom, priceTo);
        }
        return ProductMapper.toDtoList(products);
    }

    @Override
    @Transactional
    @Cacheable(cacheNames = "product",key = "#id")
    public Products GetProduct(Integer id) {
//        Cache cache = cacheManager.getCache("product");
//        Products prd = cache.get(id,Products.class);


        Products  prd = productRepository.findById(id).orElseThrow();

        return prd;
    }

    @Override
    @Cacheable(cacheNames = "product",key = "#id")

    public Integer create(ProductDto productDto) {
        Products product = Products.builder()

                .name(productDto.getName())
                .price(productDto.getPrice())
                .description(productDto.getDescription())
                .Categories(new HashSet<>())
                .build();
//        ProductDetails details = ProductDetails.builder()
//                .color(productDto.getProductDetails().getColor())
//                .imageUrl(productDto.getProductDetails().getImageUrl())
//
//                .build();
//        for (var category : productDto.getCategories()) {
//            Category categoryObject = Category.builder().name(category.getName()).products(product).build();
//            product.getCategories().add(categoryObject);
//        }
//        product.setProductDetails(details);
        productRepository.save(product);
//        Cache cache = cacheManager.getCache("product");
//        cache.put(product.getId(),product);

        return product.getId();

    }

    @Override
    @Cacheable(cacheNames = "product",key = "#id")

    public void delete(int id) {
        Products product = productRepository.findById(id).orElseThrow();


        productRepository.delete(product);
    }

    @Override
    public Authority GetAuthority(int id) {


        var result =  studentRepository.findAll();
        System.out.println(result);

        return null;
    }
}
