package az.ingress.lesson1.service;

import az.ingress.lesson1.dto.BookRequestDto;
import az.ingress.lesson1.dto.BookResponseDto;
import az.ingress.lesson1.dto.Hello;
import az.ingress.lesson1.generSpec.BookSpesification;
import az.ingress.lesson1.model.Book;
import az.ingress.lesson1.repository.BookRepository;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
@Primary
@Slf4j

public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final Hello hello;

    public BookServiceImpl(BookRepository bookRepository, Hello hello) {
        this.bookRepository = bookRepository;
        this.hello = hello;
    }
    @PostConstruct
    public  void Init(){
        log.info("Book service impl bin initialized");
    }

    @Override
    public Integer create(BookRequestDto dto) {
        Book book = Book.builder()
                .author(dto.getAuthor())
                .pageCount(dto.getPageCount())
                .name(dto.getName())
                .id(dto.getId())
                .build();
        bookRepository.save(book);
        return book.getId();
    }

    @Override
    public BookResponseDto update(Integer id, BookRequestDto dto) {
        Book book = Book.builder()
                .author(dto.getAuthor())
                .pageCount(dto.getPageCount())
                .name(dto.getName())
                .id(id)
                .build();
        bookRepository.save(book);
       var bookResponse =  BookResponseDto.builder().author(book.getAuthor())
                .pageCount(book.getPageCount())
                .name(book.getName())
                .id(book.getId()).build();
        return bookResponse;
    }

    @Override
    public void delete(Integer id) {
        Book book = bookRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Book not found"));
        bookRepository.delete(book);

    }

    @Override
    public BookResponseDto get(Integer id) {
        Book book = bookRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Book not found"));
        return BookResponseDto.builder()
                .author(book.getAuthor())
                .pageCount(book.getPageCount())
                .name(book.getName())
                .id(book.getId())
                .build();
    }

    @Override
    public List<BookResponseDto> getAll() {
        var resp = bookRepository.findAll();
        List<BookResponseDto> response = new ArrayList<BookResponseDto>();
        for(int i =0; i<resp.size();i++){
           var b =  BookResponseDto.builder()
                    .author(resp.get(i).getAuthor())
                    .pageCount(resp.get(i).getPageCount())
                    .name(resp.get(i).getName())
                    .id(resp.get(i).getId())
                    .build();
            response.add(b);

        }
        return  response;
    }

    @Override
    public Collection<Book> search(List<BookSpesification> spec) {
        return null;
    }
}
