package az.ingress.lesson1.service;

import az.ingress.lesson1.model.Account;
import az.ingress.lesson1.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AccountService {
    private final AccountRepository accountRepository;
    public  void transfer(int from, int to, int amount){
       Account fromAccout =  accountRepository.findById(from).orElseThrow();
       if(fromAccout.getBalance()< amount){
           throw new RuntimeException("Not enough balance");
       }
       Account toAccount = accountRepository.findById(to).orElseThrow();
       fromAccout.setBalance(fromAccout.getBalance()-amount);
       fromAccout.setBalance(toAccount.getBalance()+amount);
       accountRepository.save(fromAccout);
       accountRepository.save(toAccount);


    }
}
