package az.ingress.lesson1.service;

import az.ingress.lesson1.dto.ProductDto;
import az.ingress.lesson1.model.Authority;
import az.ingress.lesson1.model.Products;
import az.ingress.lesson1.model.Student;
import az.ingress.lesson1.repository.ProductProjection;

import java.util.List;

public interface ProductService {

    Integer create(ProductDto productDto);
    void  delete(int id);
    Authority GetAuthority(int id);

    List<ProductProjection> GetProductsCount();
    List<ProductDto> GetProducts(Integer priceFrom, Integer priceTo);
    Products GetProduct(Integer id);

}
