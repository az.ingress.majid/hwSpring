package az.ingress.lesson1.service;

import az.ingress.lesson1.dto.BookRequestDto;
import az.ingress.lesson1.dto.BookResponseDto;
import az.ingress.lesson1.generSpec.BookSpesification;
import az.ingress.lesson1.model.Book;

import java.util.Collection;
import java.util.List;

public interface BookService {
    Integer create(BookRequestDto dto);

    BookResponseDto update(Integer id, BookRequestDto dto);

    void delete(Integer id);

    BookResponseDto get(Integer id);
    List<BookResponseDto> getAll();

    Collection<Book> search(List<BookSpesification> spec);
}
