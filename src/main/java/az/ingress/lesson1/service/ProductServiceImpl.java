package az.ingress.lesson1.service;

import az.ingress.lesson1.dto.BookResponseDto;
import az.ingress.lesson1.dto.ProductDto;
import az.ingress.lesson1.dto.ProductMapper;
import az.ingress.lesson1.model.*;
import az.ingress.lesson1.repository.*;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.query.Page;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProductServiceImpl implements  ProductService {

    private final ProductRepository productRepository;
    private  final AuthorityRepository authorityRepository;
    private  final StudentRepository studentRepository;
    private final CacheManager cacheManager;
    private final RedisTemplate<Integer,Products> redisTemplate;



    @Override
    public List<ProductProjection> GetProductsCount() {
       return   productRepository.orderByCategory().orElseThrow(()-> new RuntimeException("not_found"));
    }

    @Override
    public List<ProductDto> GetProducts(Integer priceFrom, Integer priceTo) {
        List<Products> products;
        if (priceFrom == null && priceTo == null) {
            products=  productRepository.findAll();
            var z = products.get(5).getCategories();
            var t = 5;
        } else if (priceFrom == null) {
            products=   productRepository.findAllByPriceLessThanEqual(priceTo);
        } else if (priceTo == null) {
            products=  productRepository.findAllByPriceGreaterThanEqual(priceFrom);
        } else {
            products=  productRepository.findAllByPriceGreaterThanEqualAndPriceLessThanEqual(priceFrom, priceTo);
        }
        return ProductMapper.toDtoList(products);
    }

    @Override
    @Transactional
    public Products GetProduct(Integer id) {
//        Cache cache = cacheManager.getCache("product");
//        Products prd = cache.get(id,Products.class);

        Products prd=  redisTemplate.opsForValue().get(id);
        if(prd ==null){
             prd = productRepository.findById(id).orElseThrow();
            redisTemplate.opsForValue().set(id,prd);


        }
        return prd;
    }

    @Override
    public Integer create(ProductDto productDto) {
        Products product = Products.builder()

                .name(productDto.getName())
                .price(productDto.getPrice())
                .description(productDto.getDescription())
                .Categories(new HashSet<>())
                .build();
//        ProductDetails details = ProductDetails.builder()
//                .color(productDto.getProductDetails().getColor())
//                .imageUrl(productDto.getProductDetails().getImageUrl())
//
//                .build();
//        for (var category : productDto.getCategories()) {
//            Category categoryObject = Category.builder().name(category.getName()).products(product).build();
//            product.getCategories().add(categoryObject);
//        }
//        product.setProductDetails(details);
        productRepository.save(product);
//        Cache cache = cacheManager.getCache("product");
//        cache.put(product.getId(),product);
        redisTemplate.opsForValue().set(product.getId(),product);

        return product.getId();

    }

    @Override
    public void delete(int id) {
        Products product = productRepository.findById(id).orElseThrow();
        redisTemplate.delete(id);


        productRepository.delete(product);
    }

    @Override
    public Authority GetAuthority(int id) {


        var result =  studentRepository.findAll();
        System.out.println(result);

        return null;
    }
}
