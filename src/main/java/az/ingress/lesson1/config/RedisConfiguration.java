package az.ingress.lesson1.config;

import az.ingress.lesson1.model.Products;
import az.ingress.lesson1.model.Student;
import com.fasterxml.jackson.databind.ser.std.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnection;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class RedisConfiguration {
    @Value("${spring.data.redis.host}")
    private String host;
    @Value("${spring.data.redis.port}")

    private Integer port;
    @Bean
    public LettuceConnectionFactory redisConnectionFactory(){
        RedisStandaloneConfiguration configuration = new RedisStandaloneConfiguration();
        configuration.setHostName(host);
        configuration.setPort(port);

        return  new LettuceConnectionFactory(configuration);

    }

    @Bean
    public RedisTemplate<Integer, Products> redisTemplate(){
        final RedisTemplate<Integer, Products> template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory());

        var stringRedisSerializer = new StringRedisSerializer();
        var jacksonRedisSerializer = new GenericJackson2JsonRedisSerializer();


        template.setKeySerializer(jacksonRedisSerializer);
        template.setValueSerializer(jacksonRedisSerializer);
//        template.setHashKeySerializer(stringRedisSerializer);
//        template.setValueSerializer(jacksonRedisSerializer);
        template.afterPropertiesSet();
        return template;



    }
}
