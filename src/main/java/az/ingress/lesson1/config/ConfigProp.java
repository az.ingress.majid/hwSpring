package az.ingress.lesson1.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@ConfigurationProperties(prefix = "ms24")
@Configuration
@Data
public class ConfigProp {
    private Integer lesson;
    private List<User> users;

}
